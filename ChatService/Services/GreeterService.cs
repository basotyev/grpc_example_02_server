using Grpc.Core;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChatService
{
    public class GreeterService : Greeter.GreeterBase
    {
        private readonly ILogger<GreeterService> _logger;
        public GreeterService(ILogger<GreeterService> logger)
        {
            _logger = logger;
        }

        public override Task<HelloReply> SayHello(HelloRequest request, ServerCallContext context)
        {
            return Task.FromResult(new HelloReply
            {
                Message = "Hello " + request.Name
            });
        }
        public override Task<HelloReply2> SayHello2(HelloRequest request, ServerCallContext context)
        {
            HelloReply2 reply2 = new HelloReply2();
            reply2.Replies.Add(new HelloReply
            {
                Message = "Hello " + request.Name + " 1"
            });
            reply2.Replies.Add(new HelloReply
            {
                Message = "Salem " + request.Name + " 2"
            });
            reply2.Replies.Add(new HelloReply
            {
                Message = "Wassup " + request.Name + " 3"
            });
            return Task.FromResult(reply2);
        }
    }
}
